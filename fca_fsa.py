#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import os.path as op
import re
import pandas as pd

from bs4 import BeautifulSoup

""" Configuration for FCA FSA files
"""
# Remove quotes with less than 2 characters
MIN_QUOTE_SIZE = 3

# Folders config
ROOT = op.dirname(op.realpath(__file__))
FCA_SOURCE_HTML = op.join(ROOT, '_SOURCES', 'FCA_FSA', 'HTML_CLEAN')
FCA_SOURCE_TXT = op.join(ROOT, '_SOURCES', 'FCA_FSA', 'TXT_CLEAN')
FCA_OUTPUT = op.join(ROOT, '_OUTPUT', 'FCA_FSA')

# Text codes to replace HTML tad <i></i>
TAG_LEFT = "L__"
TAG_RIGHT = "__R"

# Authors types
PERSONS = 'Trader|Submitter|Broker|Manager|White|ROBSON'
MONTHS = 'January|February|March|April|May|June|July|August|September|October|November|December'

""" Convert HTML to clean text
"""


def convert_to_text(source_file_html):
    out = open(source_file_html, 'rt', encoding='utf-8')
    out = out.read()

    # Replace italic tags with specific text tags so they will not be removed with the HTML cleaning
    out = out.replace('<i>', TAG_LEFT)
    out = out.replace('</i>', TAG_RIGHT)

    # Remove all HTML tags
    soup = BeautifulSoup(out, "lxml")
    text = soup.get_text(' ', strip=True)

    # Remove all quotes <= MIN_QUOTE_SIZE characters
    # e.g. : L__.{1,2}__R
    text = re.sub(
        TAG_LEFT + '.{1,' + str(MIN_QUOTE_SIZE) + '}' + TAG_RIGHT, '', text)

    # Remove special characters
    text = re.sub('[“”…]', '', text)

    return text


""" Extract Quotes from sanitized text file
"""


def process_quotes(source_file_txt):
    data = open(source_file_txt, 'rt', encoding='utf-8')
    data = data.read()

    quotes = re.compile(
        r"" + TAG_LEFT + "(.*?)" + TAG_RIGHT,
        re.IGNORECASE | re.DOTALL)

    # e.g. : (paul|jane|robert|john).*?L__(.*?)__R
    # https://stackoverflow.com/questions/44266107/match-the-last-occurence-of-a-name-from-a-list-before-a-quoted-text
    quotes_persons = re.compile(
        r"(" + PERSONS + ").*?" + TAG_LEFT + "(.*?)" + TAG_RIGHT,
        re.IGNORECASE | re.DOTALL)

    quotes_dates = re.compile(
        r"(\d{1,2}\s(?:" + MONTHS + ")\s+\d{4}).*?" + TAG_LEFT + "(.*?)" + TAG_RIGHT,
        re.IGNORECASE | re.DOTALL)

    return quotes.findall(data), quotes_persons.findall(data), quotes_dates.findall(data)

# Convert HTML files to clean txt
html_files = sorted(glob.glob(FCA_SOURCE_HTML + '/*.html'))
for file in html_files:
    output = convert_to_text(file)
    file = file.replace(FCA_SOURCE_HTML, FCA_SOURCE_TXT).replace('.html', '.txt')
    with open(file, "w") as f:
        f.write(output)

# Process TXT files
txt_files = sorted(glob.glob(FCA_SOURCE_TXT + '/*.txt'))
for file in txt_files:
    quotes, quotes_persons, quotes_dates = process_quotes(file)

    df_quotes = pd.DataFrame(quotes, columns=['quotes'])
    df_quotes_persons = pd.DataFrame(quotes_persons, columns=['names', 'quotes'])
    df_quotes_dates = pd.DataFrame(quotes_dates, columns=['dates', 'quotes'])

    df_total = pd.merge(df_quotes, df_quotes_persons, how='left', on='quotes')
    df_total = pd.merge(df_total, df_quotes_dates, how='left', on='quotes')

    file = file.replace(FCA_SOURCE_TXT, FCA_OUTPUT).replace('.txt', '.csv')
    df_total.to_csv(file, index=False, header=True)


