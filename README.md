### Projet LIBOR

Le programme a pour objectif d’extraire des citations d’une série de documents ainsi que les dates et auteurs associés.

Le projet est structuré par type de documents en fonction de la manière dont les citations sont références dans les documents source.

FCA\_FSA -\> citations identifiées par des italiques.

#### Procédure manuelle :

Les documents PDF source (/\_SOURCES/ FCA\_FSA/PDF) ont été tronqués avec les seules pages contenant des citations (/\_SOURCES/ FCA\_FSA/PDF\_CLEAN). L’idée est de ne pas retenir des textes non pertinents en italiques.

Ensuite les fichiers PDF\_CLEAN ont été convertis manuellement (car aucune libraire ne faisait mieux) en fichiers HTML (/\_SOURCES// FCA\_FSA/HTML\_CLEAN)

#### Programme en Python [fca_fsa.py](fca_fsa.py) :

1) Le programme prend tous les fichiers du répertoire HTML\_CLEAN et les convertis en fichiers TXT (/\_SOURCES/TXT\_CLEAN) adaptés aux recherches par expression régulières.
-   Conversion des balises italiques **&lt;i&gt;&lt;/i&gt;** en **L\_\_** et **\_\_R** ; ceci afin de pouvoir supprimer le HTML tout en gardant les marqueurs italiques.
-   Nettoyages divers : citations de moins de 3 caractères, etc.

2) Ensuite, le programme exécute 3 expressions régulières pour extraire :
-   Les citations seules
-   Les citations avec les auteurs
-   Les citations avec les dates

3) Enfin, le programme fusionne ces 3 extractions en prenant les citations comme pivot.

Les fichiers CSV avec 3 colonnes (citations, auteurs et dates) sont alors sauvegardés dans : \_OUTPUT/FCA\_FSA 